# Cloud Images

This repository contains descriptions for building Cloud images with the [KIWI image builder](https://osinside.github.io/kiwi).

The KIWI descriptions are stored in the `kiwi-desc/` subdirectory.
Refer to the [KIWI documentation](https://osinside.github.io/kiwi/) for more details.

## Appliance build quick start

### Podman/Docker

The instructions below will use the `podman` command, but you can easily substitute for `docker`
and it should easily just work the same.

First, pull down the container of the required environment:

```bash
$ sudo podman pull registry.fedoraproject.org/fedora:34
```

Assuming you're in the root directory of the Git checkout, set up the container:

```bash
$ sudo podman run --privileged --rm -it -v $PWD:/code:z --workdir /code registry.fedoraproject.org:34 /bin/bash
```

Once in the container environment, set up your development environment and run the image build (we're using the `kiwi-desc` for this example):

```bash
# Install kiwi
[]$ dnf --assumeyes install kiwi
# Run the image build
[]$ ./kiwi-build --image-type=<TYPE> --image-profile=<PROFILE> --image-name=<DISTRO> --output-dir $PWD/tmpoutput
```

## Licensing

These descriptions are licensed under the Apache Software License, version 2.0. See `LICENSE` for details.
